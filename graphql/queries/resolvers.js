/**
 * Created by ankur at 23/3/19 5:58 PM.
 */
import readLastLines from '../../utils/read-last-lines'
import path from 'path'

// const filePath = '/home/ankur/IdeaProjects/test/tail-react-realtime-app/utils/test.txt'
const filePath=path.join(__dirname, '../../test.txt');


export const fileData = (parent, args) => {
  return readLastLines.read(filePath, args.input.line_count)
    .then((lines) => {
      console.log(lines)
      return {
        line_count: args.input.line_count,
        content: lines,
        updatedAt: new Date()
      }
    })
}
