/**
 * Created by ankur at 24/3/19 2:48 PM.
 */

import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
// import { getFileData } from '../../actions/tailActions'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';



import axios from 'axios'
// import { FILE_UPDATED, FILE_INITIAL } from '../../actions/types'



class Tail extends Component {

  constructor (props) {
    super(props)
    this.state = {
      fileData: {
        line_count: '',
        content: '',
        updatedAt: ''
      }
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  getFileData=(line_count)=>{
    axios.post('http://localhost:5000/graphql', {
      query:
        `{
          fileData(input: {line_count: ${line_count}}){
            line_count
            content
            updatedAt
          }
        }`
    })
      .then(res => {
          console.log('DATA=====>', JSON.stringify(res.data.data.fileData))

          this.setState({ fileData: res.data.data.fileData })

        }
      )
      .catch(err => {
          console.log('ERR=====>', err)
        }
      )
  }

  componentWillMount() {

  }

  componentDidMount () {
    this.getFileData(10)

  }

  componentWillReceiveProps (nextProps, nextContext) {
    if (nextProps.fileData) {
      this.setState({ fileData: nextProps.fileData })
    }
  }

  onChange (e) {
    this.state.line_count=e.target.value
  }

  onSubmit (e) {
    e.preventDefault()
    let line_count=this.state.line_count || 10
    this.getFileData(line_count)
  }

  render () {
    return (
      <div>

        <nav>
          <div className='nav-wrapper'>
            <span className='brand-logo'>TAIL</span>
          </div>
        </nav>

        <div className='row'>
          <div className='col s12 m8 offset-m2'>
            <div className='card blue-grey darken-1'>
              <div className='card-content white-text'>
                <span className='card-title'>File Contents =></span>

                  <pre>
                    <p>
                    {this.state.fileData.content}
                    </p>
                  </pre>

              </div>
              <div className='card-action white-text bold'>
                <p>Lines count: {this.state.fileData.line_count}</p>
                <p>Update time: {this.state.fileData.updatedAt}</p>
              </div>
              <form onSubmit={this.onSubmit}>
                <div className='card-content white-text row'>
                  <div className='input-field col s12'>
                    <i className='material-icons prefix'>edit</i>
                    <input
                      type='text'
                      id='line_count'
                      name='line_count'
                      placeholder='Line Count'
                      // value={this.state.fileData.line_count}
                      onChange={this.onChange}
                    />
                  </div>
                  <button className='btn waves-effect waves-light' type='submit'
                          name='action'>Submit
                    <i className='material-icons right'>send</i>
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>


      </div>
    )
  }
}

Tail.propTypes = {
  getFileData: PropTypes.func.isRequired,
  fileData: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  fileData: state.fileData
})

export default connect(mapStateToProps)(withRouter(Tail))

