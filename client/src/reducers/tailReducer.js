import {
  FILE_INITIAL, FILE_UPDATED
} from '../actions/types'

const initialState = {
  fileData: {}
}

export default function (state = initialState, action) {
  switch (action.type) {
    case FILE_INITIAL:
      return {
        ...state,
        fileData: action.payload
      }
    case FILE_UPDATED:
      return {
        ...state,
        fileData: action.payload
      }
    default:
      return state
  }
}
