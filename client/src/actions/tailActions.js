import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

let repoName = 7

// const COMMENT_QUERY = gql`
//   query fileChanged($repoName: Int!) {
//     fileChanged(repoName: $repoName) {
//       line_count
//       content
//       updatedAt
//     }
//     }
//   }
// `;

const COMMENTS_SUBSCRIPTION = gql`
  subscription fileChanged($repoName: Int!) {
    fileChanged(repoName: $repoName) {
      line_count
      content
      updatedAt
    }
  }
`

const CommentsPageWithData = ({ params }) => (

  ({ subscribeToMore, ...result }) => (
    subscribeToNewComments = () =>
      subscribeToMore({
        query: COMMENTS_SUBSCRIPTION,
        variables: { line_count: 10 },
        updateQuery: (prev, { subscriptionData }) => {
          // if (!subscriptionData.data) return prev
          const newFeedItem = subscriptionData
          console.log(subscriptionData, prev)

          return Object.assign({}, prev, {
            entry: {
              comments: [newFeedItem, ...prev.entry.comments]
            }
          })
        }
      })
  )

)
